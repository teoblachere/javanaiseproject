/***
 * JAVANAISE Implementation
 * JvnServerImpl class
 * Implementation of a Jvn server
 * Contact: 
 *
 * Authors: 
 */

package jvn;

import irc.Sentence;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.io.*;
import java.util.HashMap;


public class JvnServerImpl
        extends UnicastRemoteObject
        implements JvnLocalServer, JvnRemoteServer {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final int OBJECT_CACHE_LIMIT = 100;

    // A JVN server is managed as a singleton
    private static JvnServerImpl js = null;

    private JvnRemoteCoord remoteCoord;
    private HashMap<Integer, JvnObject> objectOfId;

    /**
     * Default constructor
     *
     * @throws JvnException
     **/
    private JvnServerImpl() throws Exception {
        super();
        Registry registry = LocateRegistry.getRegistry(2000);
        remoteCoord = (JvnRemoteCoord) registry.lookup("RemoteCoordinator");
        System.out.println("Server connected to coordinator");
        objectOfId = new HashMap<>();
    }

    /**
     * Static method allowing an application to get a reference to
     * a JVN server instance
     *
     * @throws JvnException
     **/
    public static JvnServerImpl jvnGetServer() {
        if (js == null) {
            try {
                js = new JvnServerImpl();
            } catch (Exception e) {
                return null;
            }
        }
        return js;
    }

    /**
     * The JVN service is not used anymore
     *
     * @throws JvnException
     **/
    public void jvnTerminate()
            throws jvn.JvnException {
        // to be completed
    }

    /**
     * creation of a JVN object
     *
     * @param o : the JVN object state
     * @throws JvnException
     **/
    public JvnObject jvnCreateObject(Serializable o)
            throws jvn.JvnException {
        if(o != null){
            try {
                return new JvnObjectImpl(o, remoteCoord.jvnGetObjectId());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        // to be completed
        return null;
    }

    /**
     * Associate a symbolic name with a JVN object
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @throws JvnException
     **/
    public void jvnRegisterObject(String jon, JvnObject jo)
            throws jvn.JvnException {
        try {
            // Associate object to name on coordinator server
            remoteCoord.jvnRegisterObject(jon, jo, this);
            // Get id on coord server and save it to map
            int id = jo.jvnGetObjectId();
//            objectOfId.put(id, jo);
            addJvnObjectToCache(id, jo);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Provide the reference of a JVN object beeing given its symbolic name
     *
     * @param jon : the JVN object name
     * @return the JVN object
     * @throws JvnException
     **/
    public JvnObject jvnLookupObject(String jon)
            throws jvn.JvnException {
        try {
            // Get latest object from Remote server using name
            JvnObject obj = remoteCoord.jvnLookupObject(jon, this);
            if(obj != null){
                int id = obj.jvnGetObjectId();
//                objectOfId.put(id, obj);
                addJvnObjectToCache(id, obj);
            }
            return obj;
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get a Read lock on a JVN object
     *
     * @param joi : the JVN object identification
     * @return the current JVN object state
     * @throws JvnException
     **/
    public Serializable jvnLockRead(int joi)
            throws JvnException {
        try {
            /*
             * Lock read the object with the given identifier, and get current state
             */
            Serializable obj = remoteCoord.jvnLockRead(joi, this);
            return obj;
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Get a Write lock on a JVN object
     *
     * @param joi : the JVN object identification
     * @return the current JVN object state
     * @throws JvnException
     **/
    public Serializable jvnLockWrite(int joi)
            throws JvnException {
        try {
            // Lock read the object with the given identifier
            Serializable obj = remoteCoord.jvnLockWrite(joi, this);
            return obj;
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Invalidate the Read lock of the JVN object identified by id
     * called by the JvnCoord
     *
     * @param joi : the JVN object id
     * @return void
     * @throws java.rmi.RemoteException,JvnException
     **/
    public void jvnInvalidateReader(int joi)
            throws java.rmi.RemoteException, jvn.JvnException {
        // objectOfId may not contain object if object cache has been cleared
        if(objectOfId.containsKey(joi)){
            objectOfId.get(joi).jvnInvalidateReader();
        }  else{
            throw new JvnNoObjCacheException("Cannot invalidate reader as object cache does not exist - this maybe due to it being removed during cache clearing");
        }
    }

    ;

    /**
     * Invalidate the Write lock of the JVN object identified by id
     *
     * @param joi : the JVN object id
     * @return the current JVN object state
     * @throws java.rmi.RemoteException,JvnException
     **/
    public Serializable jvnInvalidateWriter(int joi)
            throws java.rmi.RemoteException, jvn.JvnException {
        // objectOfId may not contain object if object cache has been cleared
        if(objectOfId.containsKey(joi)) {
            return objectOfId.get(joi).jvnInvalidateWriter();
        } else{
            throw new JvnNoObjCacheException("Cannot invalidate writer as object cache does not exist - this maybe due to it being removed during cache clearing");
        }
    }

    ;

    /**
     * Reduce the Write lock of the JVN object identified by id
     *
     * @param joi : the JVN object id
     * @return the current JVN object state
     * @throws java.rmi.RemoteException,JvnException
     **/
    public Serializable jvnInvalidateWriterForReader(int joi)
            throws java.rmi.RemoteException, jvn.JvnException {

        // objectOfId may not contain object if object cache has been cleared
        if(objectOfId.containsKey(joi)) {
            return objectOfId.get(joi).jvnInvalidateWriterForReader();
        } else{
            throw new JvnNoObjCacheException("Cannot invalidate writer for reader as object cache does not exist - this maybe due to it being removed during cache clearing");
        }
    }

    ;

    private void addJvnObjectToCache(int id, JvnObject jo){
        // Clear object cache before adding an object to the map if limit reached
        if(!objectOfId.containsKey(id) && (objectOfId.size()+1 > OBJECT_CACHE_LIMIT)){
            clearJvnObjectCache();
        }
        objectOfId.put(id, jo);
    }

    /*
     * Clear all jvn object caches saved. This is to prevent excessive copies of jvn objects that may no longer be needed
     */
    private void clearJvnObjectCache(){
        objectOfId.clear();
    }

}

 
