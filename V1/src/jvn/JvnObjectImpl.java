package jvn;

import java.io.Serializable;


// Jvn objet im
public class JvnObjectImpl implements JvnObject {

//    // Local Jvnserver
//    private JvnLocalServer jvnLocalServer;

    // Current lock on object
    private Lock currentLock;

    // Last object state
    private Serializable objectState;

    private int jvnObjectId;

    public JvnObjectImpl(Serializable objectState, int jvnObjectId) {
        this.objectState = objectState;
        this.jvnObjectId = jvnObjectId;
        this.currentLock = Lock.W; // For a new object - lock write is given to jvn server
    }

    /*
     * Set lock state to R
     */
    @Override
    public void jvnLockRead() throws JvnException {
        /*
         * For Lock.WC  - No need to have the coordinator server updating the object on this client - (WC = cached and still valid) we know this is the most up to date version
         * For Lock.RC  - No need to have the coordinator server updating the object on this client - (RC = cached and still valid) we know this is the most up to date version
         * For Lock.R   - Already in R state
         * For Lock.RWC - Write cache already being used for read
         */
        if(this.currentLock == Lock.WC){
            this.currentLock = Lock.RWC;
        } else if(this.currentLock == Lock.RC){
            this.currentLock = Lock.R;
        } else if(this.currentLock != Lock.R && this.currentLock != Lock.RWC) {
            JvnServerImpl server = JvnServerImpl.jvnGetServer();
            JvnObjectImpl obj = ((JvnObjectImpl) server.jvnLockRead(this.jvnObjectId));
            this.objectState = obj.jvnGetSharedObject();
            this.currentLock = Lock.R;
        }
    }

    @Override
    public void jvnLockWrite() throws JvnException {
        /*
         * For Lock.WC - No need to have the coordinator server updating the object on this client - (WC = cached and still valid) we know this is the most up to date version
         * For Lock.WC - No need to have the coordinator server updating the object on this client - (RWC = cached and still valid) we know this is the most up to date version
         * For Lock.W  - Already in W state
         */
        if(this.currentLock != Lock.W && this.currentLock != Lock.WC && this.currentLock != Lock.RWC) {
            JvnServerImpl server = JvnServerImpl.jvnGetServer();
            JvnObjectImpl obj = ((JvnObjectImpl) server.jvnLockWrite(this.jvnObjectId));
            this.objectState = obj.jvnGetSharedObject();
            this.currentLock = Lock.W;
        }
        this.currentLock = Lock.W;

    }

    /*
     * Change to cached locks once read or write is finished. We trust that this is the most up to date object
     * unless told otherwise by the coordinator
     */
    @Override
    public synchronized void jvnUnLock() throws JvnException {

        if(this.currentLock == Lock.W){
            this.currentLock = Lock.WC;
        } else if(this.currentLock == Lock.R){
            this.currentLock = Lock.RC;
        }
        notify();
    }

    @Override
    public int jvnGetObjectId() throws JvnException {
        return jvnObjectId;
    }

    @Override
    public Serializable jvnGetSharedObject() throws JvnException {
        return this.objectState;
    }

    /*
     * Put no lock as we can no longer trust that the cached object is the most up to date
     */
    @Override
    public void jvnInvalidateReader() throws JvnException {
        currentLock = Lock.NL;
    }

    /*
     * Put no lock as we can no longer trust that the cached object is the most up to date, wait until write is finished
     * (if currently writing), set to no lock and return the written object
     */
    @Override
    public synchronized Serializable jvnInvalidateWriter() throws JvnException {
        if(currentLock == Lock.W){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        currentLock = Lock.NL;
        return this;
    }

    /*
     * Another client is trying to read the object, wait until write is finished (if currently writing),
     * set to read cached and return the written object
     */
    @Override
    public synchronized Serializable jvnInvalidateWriterForReader() throws JvnException {
        if(currentLock == Lock.W){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        currentLock = Lock.RC;
        return this;
    }


    // Reset lock
    public void resetLock(){
        this.currentLock = Lock.NL;
    }
}
