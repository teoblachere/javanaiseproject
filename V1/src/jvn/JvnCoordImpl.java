/***
 * JAVANAISE Implementation
 * JvnCoordImpl class
 * This class implements the Javanaise central coordinator
 * Contact:  
 *
 * Authors: 
 */

package jvn;

import java.rmi.ConnectException;
import java.rmi.server.UnicastRemoteObject;
import java.io.Serializable;
import java.util.*;

public class JvnCoordImpl
        extends UnicastRemoteObject
        implements JvnRemoteCoord {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int lastIdGiven;
    private HashMap<String, Integer> idOfObjectName;
    private HashMap<Integer, JvnObject> objectOfId;
    private HashMap<Integer, HashMap<JvnRemoteServer, Lock>> serverLocksOfObject;

    /**
     * Default constructor
     *
     * @throws JvnException
     **/
    public JvnCoordImpl() throws Exception {
        lastIdGiven = 0;
        serverLocksOfObject = new HashMap<>();
        objectOfId = new HashMap<>();
        idOfObjectName = new HashMap<>();
    }

    /**
     * Allocate a NEW JVN object id (usually allocated to a
     * newly created JVN object)
     *
     * @throws java.rmi.RemoteException,JvnException
     **/
    public int jvnGetObjectId()
            throws java.rmi.RemoteException, jvn.JvnException {
        lastIdGiven++;
        return lastIdGiven;
    }

    /**
     * Associate a symbolic name with a JVN object
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @param js  : the remote reference of the JVNServer
     * @throws java.rmi.RemoteException,JvnException
     **/
    public void jvnRegisterObject(String jon, JvnObject jo, JvnRemoteServer js)
            throws java.rmi.RemoteException, jvn.JvnException {
        // Generate new id
        Integer id = jo.jvnGetObjectId();
        // Associate object to new id in class map e.g. 2323 -> JvnObject{...}
        jo.resetLock(); // lock should be defaulted, ready for a fetch from a new server. lock for js is stored in the serverLocksOfObject
        objectOfId.put(id, jo);
        // Associate id to object name in class map e.g. IRC -> 2323
        idOfObjectName.put(jon, id);
        // The jvn server that created the object has write lock on the object to start with
        serverLocksOfObject.put(id, new HashMap<>());
        serverLocksOfObject.get(id).put(js, Lock.W);
        System.out.println("Registered object " + jon + " with id " + id.toString());
    }

    /**
     * Get the reference of a JVN object managed by a given JVN server
     *
     * @param jon : the JVN object name
     * @param js  : the remote reference of the JVNServer
     * @throws java.rmi.RemoteException,JvnException
     **/
    public JvnObject jvnLookupObject(String jon, JvnRemoteServer js)
            throws java.rmi.RemoteException, jvn.JvnException {
        // Get latest id for Object name
        Integer id = idOfObjectName.get(jon);
        if(id != null){
            // Using latest id, return associated object
            return objectOfId.get(id);
        }else{
            return null;
        }
    }

    /**
     * Get a Read lock on a JVN object managed by a given JVN server
     *
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws java.rmi.RemoteException, JvnException
     **/
    public Serializable jvnLockRead(int joi, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {

        /*
         * If there has been no previous lock on the object, add object id to lock hash map with client
         */
        if(!serverLocksOfObject.containsKey(joi)){
            serverLocksOfObject.put(joi, new HashMap<>());
            serverLocksOfObject.get(joi).put(js, Lock.R);
            /*
             * Return latest state
             */
            if(objectOfId.containsKey(joi)){
                return objectOfId.get(joi); // No need to invalidate for reader as no previous lock in map
            } else{
                throw new JvnException("Object could not be found with Id - " + joi);
            }
        }

        HashMap<JvnRemoteServer, Lock> locks = serverLocksOfObject.get(joi);
        Set<JvnRemoteServer> toRemove = new HashSet<>();

        for(JvnRemoteServer server: locks.keySet()){
            if(!server.equals(js)){
                /*
                 * On lock read - if another client has Lock.W (lock write) -> get the object saved on their server and update the objects state
                 */
                /*
                 * On lock read, invalidate other client writers for reader
                 * If this is the case:
                 * - we need wait to get this updated object, update the object in this class and return the
                 *   updated object to the client server calling this method
                 */
                switch(locks.get(server)){
                    case W:
                        try{
                            JvnObject obj = (JvnObject)server.jvnInvalidateWriterForReader(joi); // Handle client
                            objectOfId.put(joi, obj); // Update object saved on coord server
                            locks.put(server, Lock.R); // Write lock becomes read lock
                        } catch (JvnNoObjCacheException cacheException){
                            System.out.println(cacheException.message);
                            toRemove.add(server);
                        } catch (ConnectException connectException){
                            System.out.println("Client connection could not be established during Coord Read Lock " + connectException.getMessage());
                            toRemove.add(server); // Can no longer connect to client server - lock therefore removed
                        }

                        break;
                }
            }
        }
        locks.keySet().removeAll(toRemove); // Avoids ConcurrentModificationException when removing during iteration


        /*
         * Update server locks map with read lock for client server
         */
        locks.put(js, Lock.R);

        /*
         * Return jvn object associated to id
         */
        if(objectOfId.containsKey(joi)){
            return objectOfId.get(joi);
        } else{
            throw new JvnException("Object not found during jvnLockRead on coord server, object Id = " + joi);
        }
    }

    /**
     * Get a Write lock on a JVN object managed by a given JVN server
     *
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws java.rmi.RemoteException, JvnException
     **/
    public Serializable jvnLockWrite(int joi, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {

        /*
         * If there has been no previous lock on the object, add object id to lock hash map with client
         */
        if(!serverLocksOfObject.containsKey(joi)){
            serverLocksOfObject.put(joi, new HashMap<>());
            serverLocksOfObject.get(joi).put(js, Lock.W);
            /*
             * Return latest state
             */
            if(objectOfId.containsKey(joi)){
                return objectOfId.get(joi); // No need to invalidate for reader as no previous lock in map
            } else{
                throw new JvnException("Object could not be found with Id - " + joi);
            }
        }

        HashMap<JvnRemoteServer, Lock> locks = serverLocksOfObject.get(joi);
        Set<JvnRemoteServer> toRemove = new HashSet<>();
        /*
         * For other client servers, we need to remove the lock they have of the object as the calling client server now has the W lock
         */
        for(JvnRemoteServer server: locks.keySet()){
            /*
             * On lock write, invalidate other client readers and writers
             * For the case of Lock.W on another client server, the object is being updated
             * - therefore we need wait to get this updated object, update the object on this class and return the
             *   updated object to the client server calling this method
             */
            if(!server.equals(js)){
                switch(locks.get(server)){
                    case R:
                        try{
                            server.jvnInvalidateReader(joi);
                        } catch (JvnNoObjCacheException cacheException){
                            System.out.println(cacheException.message);
                        } catch (ConnectException connectException){
                            System.out.println("Client connection could not be established during Coord write Lock (invalidate reader) " + connectException.getMessage());
                            toRemove.add(server); // Can no longer connect to client server - lock therefore removed
                        }

                        toRemove.add(server);
                        break;
                    case W:
                        try{
                            JvnObject obj = (JvnObject)server.jvnInvalidateWriter(joi);
                            objectOfId.put(joi, obj);
                        } catch (JvnNoObjCacheException cacheException){
                            System.out.println(cacheException.message);
                        } catch (ConnectException connectException){
                            System.out.println("Client connection could not be established during Coord write Lock (invalidate reader) " + connectException.getMessage());
                            toRemove.add(server); // Can no longer connect to client server - lock therefore removed
                        }
                        toRemove.add(server);
                        break;

                }
            }
        }
        locks.keySet().removeAll(toRemove); // Avoids ConcurrentModificationException when removing during iteration

        locks.put(js, Lock.W); // Update server locks map with write lock for client server

        if(objectOfId.containsKey(joi)){
            return objectOfId.get(joi); // Return jvn object associated to id
        } else{
            throw new JvnException("Object not found during jvnLockWrite on coord server, object Id = " + joi);
        }
    }


    /**
     * A JVN server terminates
     *
     * @param js : the remote reference of the server
     * @throws java.rmi.RemoteException, JvnException
     **/
    public void jvnTerminate(JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {

    }
}

 
