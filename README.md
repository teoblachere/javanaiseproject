#JavanaiseProject

## Folders
* V1 - JVN1, <b>Generic</b> interception objects
* V2 - JVN2, Automatic generation of <b>specific</b> interception objects (Dynamic proxy)
* <em><b>See V2 folder for most up to date jvn code</b></em> (for all extensions, synchronisation management etc...)

## Running project

1. Run the CoordServer [`java -classpath ${ProjectHome}/bin irc.CoordServer`], the string 'Coordinator is ready' will be printed if the RMI registry has been created and the coordinator server is binded
2. Run Irc [`java -classpath ${ProjectHome}/bin irc.Irc`]

## Running Burst Tests

1. Run the CoordServer [`java -classpath ${ProjectHome}/bin irc.CoordServer`], the string 'Coordinator is ready' will be printed if the RMI registry has been created and the coordinator server is binded
2. Run BurstTest [`java -classpath ${ProjectHome}/bin Tests.BurstTest`]

## Components

### Application (IRC)

### Object (Sentence)

### Object Interceptor (JvnSentence)
* implements **JvnObject** so that the application can use it for an object interceptor


### Jvn application Server (JvnServerImpl)
* implements **JvnLocalServer** so that the application and the object interceptor can communicate with it
* implements **JvnRemoteServer** so that the coordinator can communicate with it 

### Coordinator Server (JvnCoordImpl)
* implements **JvnRemoteCoord** so that the Jvn application server can communicate with it

## Monitoring
* <b>Local server</b>: Create/Read/Update operations are synchronized on the JVNObject methods rather than the JvnLocalServer methods - no need to synchronise a method in the local server as they could be using different unrelated local objects even in the same method
    * Other specific parts are the code are also synchronised (see commenting in class for more info
* <b>Coord server</b>: Methods in this class could be using the same object for the same instance. 
    * To make sure they are on the same object - synchronized blocks are used passing in the object in question.
    * This avoids unnecessary synchronisation (and therefore unnecessary loss of performance)
## JVN server termination
A jvn server can be terminated by calling the jvnTerminate method in the jvnCoordImpl class.
* If the jvn server has a write lock on an object. The coord server invalidates the writer on the client to make sure the coord server has the latest cached object before the termination of the server.


## Extensions
1. Clearing client cache - changes
* JvnServerImpl.clearJvnObjectCache() - this clears all object cache for the local client server. Is called every time the JvnServerImpl.OBJECT_CACHE_LIMIT is reached
    * If the object is transaction bound (Lock not cached), the object cannot be removed from the cache as it is being used. The jvn server does not wait until it's cached (e.g. W -> WC) before removing it from the cache as:
        1. As an active lock is on the object, it's not a stale object (e.g. an object that has not been read/updated for a long time)
        2. It would reduce performance if we waited for the Lock.W to become Lock.WC as the cache clear would have to wait for all objects to be cached before removing it.   
* jvnInvalidateReader/jvnInvalidateWriter/jvnInvalidateWriterForReader check if the cache is  still stored when the coordinator calls these methods. If not - a JvnNoObjCacheException is thrown
    * The coord server has to deal with these exceptions
        * The client is removed from the cache map saved on the coord server (serverLocksOfObject)
        * The object returned is the last object saved on the coord server 
* Single thread assumed for this functionality 
2. Broken client
* If client broken - When jvn remote server methods are called (see CoordServer), the broken client server connection issue is handled by the ConnectException block
    * The client is removed from the cache map saved on the coord server (serverLocksOfObject) as client assumed to be broken.
    * As there is no cache map saved on the coord server for the jvn object the coord never calls invalidatewriter/invalidatereader again for the client. This means:
        * The coord server can gain performance on a jvnlockread/write by not having to try to call the client server (assuming it's broken)
        * But if the client server is not actually broken, the client will think that a cached object is always up to date as there is never a call from a the coord server when another client tries to update the object. 
3. Broken coord server
* If coord server is broken, a retry is made after waiting 1 second. If still broken - we can assume it will be broken for some time. 
* If coord server is broken - no client can communicate with it. 
    * Therefore locks on the objects cannot be obtained. 
    * Locks remain the same on client objects (with the knowledge that invalidate methods can not be called on any of the other jvn client servers if the coord server is broken).
4. Burst tests 
* Test (BurstTest.NB_CLIENTS) clients with (BurstTest.NB_OBJECTS) objects that read or write (BurstTest.NB_INSTRUCTIONS) times. These constants can be modified.
* Observations
    * If the number of objects is above the cache limit of the jvn server (see JvnServerImpl.OBJECT_CACHE_LIMIT), this can increase the time of test execution significantly
    * If one client process has terminated with a write cache on an object whilst another client tries to write/read the latest object, an error is thrown in the logs of the coord server - this is expected as the client has been terminated without telling the coord server.
    * Deadlocks can be shown to stop the test from running until the end if the jvn implementations have not been properly synchronised