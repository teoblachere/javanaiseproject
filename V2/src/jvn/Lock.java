package jvn;

public enum Lock {
    NL, //No Lock
    RC, //Read lock Cached
    WC, //Write lock Cached
    R, //Read lock
    W, //Write lock
    RWC //Write lock Cached & Read taken
}
