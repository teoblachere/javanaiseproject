package jvn;

public class JvnNoObjCacheException extends JvnException {
    JvnNoObjCacheException(String message){
        super(message);
    }
}
