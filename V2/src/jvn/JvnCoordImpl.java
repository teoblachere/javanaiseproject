/***
 * JAVANAISE Implementation
 * JvnCoordImpl class
 * This class implements the Javanaise central coordinator
 * Contact:  
 *
 * Authors: 
 */

package jvn;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class JvnCoordImpl
        extends UnicastRemoteObject
        implements JvnRemoteCoord {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int lastIdGiven;
    private HashMap<String, Integer> idOfObjectName;
    private HashMap<Integer, JvnObject> objectOfId;
    private ConcurrentHashMap<Integer, HashMap<JvnRemoteServer, Lock>> serverLocksOfObject; // Concurrent hash map means that for an object the locks for each server are always coherent

    /**
     * Default constructor
     *
     * @throws JvnException
     **/
    public JvnCoordImpl() throws Exception {
        lastIdGiven = 0;
        serverLocksOfObject = new ConcurrentHashMap<>();
        objectOfId = new HashMap<>();
        idOfObjectName = new HashMap<>();
    }

    /**
     * Allocate a NEW JVN object id (usually allocated to a
     * newly created JVN object)
     *
     * @throws java.rmi.RemoteException,JvnException
     **/
    public synchronized int jvnGetObjectId() // Synchronized to make sure one Id is given at a time and the id is incremented by one each time
            throws java.rmi.RemoteException, jvn.JvnException {
        lastIdGiven++;
        return lastIdGiven;
    }

    /**
     * Associate a symbolic name with a JVN object
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @param js  : the remote reference of the JVNServer
     * @throws java.rmi.RemoteException,JvnException
     **/
    public synchronized void jvnRegisterObject(String jon, JvnObject jo, JvnRemoteServer js)
            throws java.rmi.RemoteException, jvn.JvnException {
        // Generate new id
        Integer id = jo.jvnGetObjectId();
        // Associate object to new id in class map e.g. 2323 -> JvnObject{...}
        jo.resetLock(); // lock should be defaulted, ready for a fetch from a new server. lock for js is stored in the serverLocksOfObject
        objectOfId.put(id, jo);
        // Associate id to object name in class map e.g. IRC -> 2323
        idOfObjectName.put(jon, id);
        // The jvn server that created the object has write lock on the object to start with
        serverLocksOfObject.put(id, new HashMap<>());
        serverLocksOfObject.get(id).put(js, Lock.W);
        System.out.println("Registered object " + jon + " with id " + id.toString());
    }

    /**
     * Get the reference of a JVN object managed by a given JVN server
     *
     * @param jon : the JVN object name
     * @param js  : the remote reference of the JVNServer
     * @throws java.rmi.RemoteException,JvnException
     **/
    public synchronized JvnObject jvnLookupObject(String jon, JvnRemoteServer js)
            throws java.rmi.RemoteException, jvn.JvnException {
        // Get latest id for Object name
        Integer id = idOfObjectName.get(jon);
        if (id != null) {
            return objectOfId.get(id);
            // Using latest id, return associated object
        } else {
            return null;
        }
    }


    private HashMap<JvnRemoteServer, Lock> getOrCreateServerLocksOfSingleObject(int joi) {
        /*
         * If there has been no previous lock on the object, add object id to lock hash map with client
         * This operation is thread safe (locks the keys region) - ensures that only one new segment is created for the joi key if absent
         * The existing or new value is returned
         */
        serverLocksOfObject.putIfAbsent(joi, new HashMap<>());
        return serverLocksOfObject.get(joi);
    }

    /**
     * Get a Read lock on a JVN object managed by a given JVN server
     *
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws java.rmi.RemoteException, JvnException
     **/
    public Serializable jvnLockRead(int joi, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {

        System.out.println("Lock read on coord for " + js.toString() + " joi = " + joi);


        // Synchronising on HashMap<JvnRemoteServer, Lock> object for one serverLocksOfObject joi key
        synchronized (getOrCreateServerLocksOfSingleObject(joi)) { // Lock on mutable object for key

            //System.out.println("joi lock read for server - " + js);


            HashMap<JvnRemoteServer, Lock> locks = serverLocksOfObject.get(joi); // the getOrCreateServerLocksOfSingleObject ensures this key exists
            Set<JvnRemoteServer> toRemove = new HashSet<>();
            for (JvnRemoteServer server : locks.keySet()) {
                if (!server.equals(js)) {
                    //System.out.println("has server not the same for lock read");
                    /*
                     * On lock read - if another client has Lock.W (lock write) -> get the object saved on their server and update the objects state
                     */
                    /*
                     * On lock read, invalidate other client writers for reader
                     * If this is the case:
                     * - we need wait to get this updated object, update the object in this class and return the
                     *   updated object to the client server calling this method
                     */
                    switch (locks.get(server)) {
                        case W:
                            try {
                                JvnObject obj = (JvnObject) server.jvnInvalidateWriterForReader(joi); // Handle client
                                objectOfId.put(joi, obj); // Update object saved on coord server
                                locks.put(server, Lock.R); // Write lock becomes read lock
                            } catch (JvnNoObjCacheException cacheException) {
                                System.out.println(cacheException.message);
                                toRemove.add(server); // Remove as object is not cached on server
                            } catch (RemoteException re) {
                                System.out.println("(jvnLockRead) Client connection could not be established during Coord Read Lock - retry ");
                                try {
                                    Thread.sleep(500); // wait half a second before retrying
                                    JvnObject obj = (JvnObject) server.jvnInvalidateWriterForReader(joi); // Handle client
                                    objectOfId.put(joi, obj); // Update object saved on coord server
                                    locks.put(server, Lock.R); // Write lock becomes read lock
                                } catch (InterruptedException ie) {
                                    // Restore the interrupted status (otherwise this state is cleared)
                                    Thread.currentThread().interrupt();
                                    toRemove.add(server); // Can no longer connect to client server - lock therefore removed
                                } catch (RemoteException ce2) {
                                    System.out.println("(jvnLockRead) Client connection could not be established during Coord Read Lock - assumed broken ");
                                    ce2.printStackTrace();
                                    toRemove.add(server); // Can no longer connect to client server - lock therefore removed
                                }
                            }

                            break;
                    }
                }
            }
            locks.keySet().removeAll(toRemove); // Avoids ConcurrentModificationException when removing during iteration


            /*
             * Update server locks map with read lock for client server
             */
            locks.put(js, Lock.R);


            System.out.println("joi lock read end for server - " + js);

            /*
             * Return jvn object associated to id
             */
            if (objectOfId.containsKey(joi)) {
                return objectOfId.get(joi);
            } else {
                throw new JvnException("Object not found during jvnLockRead on coord server, object Id = " + joi);
            }
        }

    }

    /**
     * Get a Write lock on a JVN object managed by a given JVN server
     *
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws java.rmi.RemoteException, JvnException
     **/
    public Serializable jvnLockWrite(int joi, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {

        System.out.println("Lock write on coord for " + js + " joi = " + joi);


        // Synchronising on HashMap<JvnRemoteServer, Lock> object for one serverLocksOfObject joi key
        synchronized (getOrCreateServerLocksOfSingleObject(joi)) {

            //System.out.println("joi lock write for server - " + js);
            HashMap<JvnRemoteServer, Lock> locks = serverLocksOfObject.get(joi); // the getOrCreateServerLocksOfSingleObject ensures this key exists
            Set<JvnRemoteServer> toRemove = new HashSet<>();
            /*
             * For other client servers, we need to remove the lock they have of the object as the calling client server now has the W lock
             */
            for (JvnRemoteServer server : locks.keySet()) {
                /*
                 * On lock write, invalidate other client readers and writers
                 * For the case of Lock.W on another client server, the object is being updated
                 * - therefore we need wait to get this updated object, update the object on this class and return the
                 *      updated object to the client server calling this method
                 */
                if (!server.equals(js)) {
                    //System.out.println("has server not the same for lock write");
                    switch (locks.get(server)) {
                        case R:
                            try {
                                server.jvnInvalidateReader(joi);
                            } catch (JvnNoObjCacheException cacheException) {
                                System.out.println(cacheException.message);
                            } catch (RemoteException re) {
                                System.out.println("(jvnLockWrite) Client connection could not be established during Coord write Lock - retry ");
                                try {
                                    Thread.sleep(500); // wait half a second before retrying
                                    server.jvnInvalidateReader(joi);
                                } catch (InterruptedException ie) {
                                    // Restore the interrupted status (otherwise this state is cleared)
                                    Thread.currentThread().interrupt();
                                    toRemove.add(server); // Can no longer connect to client server - lock therefore removed
                                } catch (RemoteException ce2) {
                                    System.out.println("(jvnLockWrite) Client connection could not be established during Coord write Lock - assumed broken ");
                                    ce2.printStackTrace();
                                    toRemove.add(server); // Can no longer connect to client server - lock therefore removed
                                }
                            }
                            toRemove.add(server);
                            break;
                        case W:
                            try {
                                JvnObject obj = (JvnObject) server.jvnInvalidateWriter(joi);
                                objectOfId.put(joi, obj);
                            } catch (JvnNoObjCacheException cacheException) {
                                System.out.println(cacheException.message);
                            } catch (RemoteException re) {
                                System.out.println("(jvnLockWrite) Client connection could not be established during Coord write Lock - retry ");
                                try {
                                    Thread.sleep(500); // wait half a second before retrying
                                    JvnObject obj = (JvnObject) server.jvnInvalidateWriter(joi);
                                    objectOfId.put(joi, obj);
                                } catch (InterruptedException ie) {
                                    // Restore the interrupted status (otherwise this state is cleared)
                                    Thread.currentThread().interrupt();
                                    toRemove.add(server); // Can no longer connect to client server - lock therefore removed
                                } catch (RemoteException ce2) {
                                    System.out.println("(jvnLockWrite) Client connection could not be established during Coord write Lock - assumed broken ");
                                    ce2.printStackTrace();
                                    toRemove.add(server); // Can no longer connect to client server - lock therefore removed
                                }
                            }
                            toRemove.add(server);
                            break;

                    }
                }
            }
            locks.keySet().removeAll(toRemove); // Avoids ConcurrentModificationException when removing during iteration

            locks.put(js, Lock.W); // Update server locks map with write lock for client server
            System.out.println("joi lock write end for server - " + js);

            if (objectOfId.containsKey(joi)) {
                return objectOfId.get(joi); // Return jvn object associated to id
            } else {
                throw new JvnException("Object not found during jvnLockWrite on coord server, object Id = " + joi);
            }

        }
    }


    /**
     * A JVN server terminates
     *
     * @param js : the remote reference of the server
     * @throws java.rmi.RemoteException, JvnException
     **/
    public synchronized void jvnTerminate(JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {
        // Remove all references to js server.
        for (Integer jvnObjectId : serverLocksOfObject.keySet()) {
            if (serverLocksOfObject.get(jvnObjectId).containsKey(js)) {
                synchronized (serverLocksOfObject.get(jvnObjectId)) {
                    //  If any objects are cached and written, fetch the object to get most up to date version before removing reference to jvn server
                    if (serverLocksOfObject.get(jvnObjectId).get(js) == Lock.W) {
                        try {
                            JvnObject obj = (JvnObject) js.jvnInvalidateWriter(jvnObjectId);
                            objectOfId.put(jvnObjectId, obj);
                        } catch (JvnNoObjCacheException cacheException) {
                            System.out.println(cacheException.message);
                        } catch (RemoteException re) {
                            System.out.println("(jvnTerminate) Client connection could not be established during Coord write Lock - retry ");
                            try {
                                Thread.sleep(500); // wait half a second before retrying
                                JvnObject obj = (JvnObject) js.jvnInvalidateWriter(jvnObjectId);
                                objectOfId.put(jvnObjectId, obj);
                            } catch (InterruptedException ie) {
                                // Restore the interrupted status (otherwise this state is cleared)
                                Thread.currentThread().interrupt();
                            } catch (RemoteException ce2) {
                                System.out.println("(jvnTerminate) Client connection could not be established during Coord write Lock - assumed broken ");
                                ce2.printStackTrace();
                            }
                        }
                    }
                }
                serverLocksOfObject.get(jvnObjectId).remove(js);

            }
        }

    }


}

 
