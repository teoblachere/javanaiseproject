package jvn;

import java.io.Serializable;


// Jvn objet im
public class JvnObjectImpl implements JvnObject {

//    // Local Jvnserver
//    private JvnLocalServer jvnLocalServer;

    // Current lock on object
    private Lock currentLock;

    // Last object state
    private Serializable objectState;

    private int jvnObjectId;

    public JvnObjectImpl(Serializable objectState, int jvnObjectId) {
        this.objectState = objectState;
        this.jvnObjectId = jvnObjectId;
        this.currentLock = Lock.W; // For a new object - lock write is given to jvn server
    }

    /*
     * Set lock state to R
     */
    @Override
    public synchronized void jvnLockRead() throws JvnException {
        /*
         * For Lock.WC  - No need to have the coordinator server updating the object on this client - (WC = cached and still valid) we know this is the most up to date version
         * For Lock.RC  - No need to have the coordinator server updating the object on this client - (RC = cached and still valid) we know this is the most up to date version
         * For Lock.R   - Already in R state
         * For Lock.RWC - Write cache already being used for read
         */
        JvnServerImpl server = JvnServerImpl.jvnGetServer();
        if(!server.jvnObjectCached(this.jvnObjectId)){
            JvnObjectImpl obj = ((JvnObjectImpl) server.jvnLockRead(this.jvnObjectId));
            this.objectState = obj.jvnGetSharedObject();
            this.currentLock = Lock.R;
            server.addJvnObjectToCache(this.jvnObjectId, this);
        } else{
            if(this.currentLock == Lock.WC){
                this.currentLock = Lock.RWC;
            } else if(this.currentLock == Lock.RC){
                this.currentLock = Lock.R;
            } else if(this.currentLock != Lock.R && this.currentLock != Lock.RWC) {
//            JvnServerImpl server = JvnServerImpl.jvnGetServer();
                JvnObjectImpl obj = ((JvnObjectImpl) server.jvnLockRead(this.jvnObjectId));
                this.objectState = obj.jvnGetSharedObject();
                this.currentLock = Lock.R;
            }
        }


    }

    @Override
    public synchronized void jvnLockWrite() throws JvnException {
        /*
         * For Lock.WC - No need to have the coordinator server updating the object on this client - (WC = cached and still valid) we know this is the most up to date version
         * For Lock.RWC - No need to have the coordinator server updating the object on this client - (RWC = cached and still valid) we know this is the most up to date version
         * For Lock.W  - Already in W state
         */
        JvnServerImpl server = JvnServerImpl.jvnGetServer();
        if(!server.jvnObjectCached(this.jvnObjectId)){
            JvnObjectImpl obj = ((JvnObjectImpl) server.jvnLockRead(this.jvnObjectId));
            this.objectState = obj.jvnGetSharedObject();
            this.currentLock = Lock.W;
            server.addJvnObjectToCache(this.jvnObjectId, this);
        } else{
            if(this.currentLock != Lock.W && this.currentLock != Lock.WC && this.currentLock != Lock.RWC) {
                JvnObjectImpl obj = ((JvnObjectImpl) server.jvnLockWrite(this.jvnObjectId));
                this.objectState = obj.jvnGetSharedObject();
                this.currentLock = Lock.W;
            }
            this.currentLock = Lock.W;
        }


    }

    /*
     * Change to cached locks once read or write is finished. We trust that this is the most up to date object
     * unless told otherwise by the coordinator
     */
    @Override
    public synchronized void jvnUnLock() throws JvnException {

        if(this.currentLock == Lock.W){
            this.currentLock = Lock.WC;
        } else if(this.currentLock == Lock.R){
            this.currentLock = Lock.RC;
        }
        notify();
    }

    @Override
    public synchronized int jvnGetObjectId() throws JvnException {
        return jvnObjectId;
    }

    @Override
    public synchronized Serializable jvnGetSharedObject() throws JvnException {
        return this.objectState;
    }

    /*
     * Put no lock as we can no longer trust that the cached object is the most up to date
     */
    @Override
    public void jvnInvalidateReader() throws JvnException {
        currentLock = Lock.NL;
    }

    /*
     * Put no lock as we can no longer trust that the cached object is the most up to date, wait until write is finished
     * (if currently writing), set to no lock and return the written object
     */
    @Override
    public Serializable jvnInvalidateWriter() throws JvnException {
        while(currentLock == Lock.W){
            // If lock W, wait until lock updated before invalidating the writer
            synchronized (this){
                try {
                    wait();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    throw new JvnException("(jvnInvalidateWriter) InterruptedException (see stack trace)");
                }
            }
        }
        currentLock = Lock.NL;
        return this;
    }

    /*
     * Another client is trying to read the object, wait until write is finished (if currently writing),
     * set to read cached and return the written object
     */
    @Override
    public  Serializable jvnInvalidateWriterForReader() throws JvnException {
        while(currentLock == Lock.W){
            // If lock W, wait until lock updated before invalidating the writer
            synchronized (this){
                try {
                    wait();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    throw new JvnException("(jvnInvalidateWriterForReader) InterruptedException (see stack trace)");
                }
            }

        }
        currentLock = Lock.RC;
        return this;
    }

    @Override
    public boolean isTransactionBound() {
        return (Lock.W.equals(currentLock) || Lock.R.equals(currentLock));
    }

    @Override
    public void resetLock(){
        this.currentLock = Lock.NL;
    }
}
