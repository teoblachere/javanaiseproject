/***
 * JAVANAISE Implementation
 * JvnServerImpl class
 * Implementation of a Jvn server
 * Contact: 
 *
 * Authors: 
 */

package jvn;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;


public class JvnServerImpl
        extends UnicastRemoteObject
        implements JvnLocalServer, JvnRemoteServer {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final int OBJECT_CACHE_LIMIT = 2;
//    private static final int OBJECT_CACHE_LIMIT = 1;

    // A JVN server is managed as a singleton
    private static JvnServerImpl js = null;

    private JvnRemoteCoord remoteCoord;
    private HashMap<Integer, JvnObject> objectOfId;

    /**
     * Default constructor
     *
     * @throws JvnException
     **/
    private JvnServerImpl() throws Exception {
        super();
        Registry registry = LocateRegistry.getRegistry(2000);
        remoteCoord = (JvnRemoteCoord) registry.lookup("RemoteCoordinator");
        System.out.println("Server connected to coordinator");
        objectOfId = new HashMap<>();
    }

    /**
     * Static method allowing an application to get a reference to
     * a JVN server instance
     *
     * @throws JvnException
     **/
    public static JvnServerImpl jvnGetServer() throws JvnException {
        if (js == null) {
            try {
                js = new JvnServerImpl();
            } catch (Exception e) {
                e.printStackTrace();
                throw new JvnException("Exception creating new JvnServerImpl instance (see stack trace)");
            }
        }
        return js;
    }

    /**
     * The JVN service is not used anymore
     *
     * @throws JvnException
     **/
    public void jvnTerminate()
            throws jvn.JvnException {
        try{
            // Coord is notified to not use this server anymore
            remoteCoord.jvnTerminate(this);
        } catch (RemoteException e){
            System.out.println("(jvnTerminate) Problem connecting to the coord server, this could be due to overloaded queue or broken remote server - retrying again");
            try {
                Thread.sleep(500);
                remoteCoord.jvnTerminate(this);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
                // Restore the interrupted status (otherwise this state is cleared)
                Thread.currentThread().interrupt();
                throw new JvnException("(jvnTerminate) InterruptedException (see stack trace)");
            } catch (RemoteException re2) {
                re2.printStackTrace();
                throw new JvnException("(jvnTerminate) Problem connecting to the coord server after retry - assumed broken (see stack trace)");
            }
        } finally {
            js = null; // When jvnGetServer is called from the application, a new jvnserver instance is created
        }
    }

    /**
     * creation of a JVN object
     *
     * @param o : the JVN object state
     * @throws JvnException
     **/
    public JvnObject jvnCreateObject(Serializable o)
            throws jvn.JvnException {
        if (o != null) {
            try {
                return new JvnObjectImpl(o, remoteCoord.jvnGetObjectId());
            } catch (RemoteException e) {
                System.out.println("(jvnCreateObject) Problem connecting to the coord server, this could be due to overloaded queue or broken remote server - retrying again");
                try {
                    Thread.sleep(100);
                    return new JvnObjectImpl(o, remoteCoord.jvnGetObjectId());
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                    // Restore the interrupted status (otherwise this state is cleared)
                    Thread.currentThread().interrupt();
                    throw new JvnException("(jvnCreateObject) InterruptedException (see stack trace)");
                } catch (RemoteException re2) {
                    re2.printStackTrace();
                    throw new JvnException("(jvnCreateObject) Problem connecting to the coord server after retry - assumed broken (see stack trace)");
                }

            }
        } else{
            throw new JvnException("(jvnCreateObject) - cannot create null object");
        }
    }

    /**
     * Associate a symbolic name with a JVN object
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @throws JvnException
     **/
    public void jvnRegisterObject(String jon, JvnObject jo)
            throws jvn.JvnException {
        try {
            registerObject(jon, jo);
        } catch (RemoteException e) {
            System.out.println("(jvnRegisterObject) Problem connecting to the coord server, this could be due to overloaded queue or broken remote server - retrying again");
            try {
                Thread.sleep(100);
                registerObject(jon, jo);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
                // Restore the interrupted status (otherwise this state is cleared)
                Thread.currentThread().interrupt();
                throw new JvnException("(jvnRegisterObject) InterruptedException (see stack trace)");
            } catch (RemoteException re2) {
                re2.printStackTrace();
                throw new JvnException("jvnRegisterObject) Problem connecting to the coord server - assumed broken");
            }
        }
    }

    /**
     * Provide the reference of a JVN object beeing given its symbolic name
     *
     * @param jon : the JVN object name
     * @return the JVN object
     * @throws JvnException
     **/
    public JvnObject jvnLookupObject(String jon)
            throws jvn.JvnException {
        try {
            // Get latest object from Remote server using name
            return lookupObjectOnCoordServer(jon);
        } catch (RemoteException re) {
            System.out.println("(jvnLookupObject) Problem connecting to the coord server, this could be due to overloaded queue or broken remote server - retrying again");
            try {
                Thread.sleep(100);
                return lookupObjectOnCoordServer(jon);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
                // Restore the interrupted status (otherwise this state is cleared)
                Thread.currentThread().interrupt();
                throw new JvnException("(jvnLookupObject) InterruptedException (see stack trace)");
            } catch (RemoteException re2) {
                re2.printStackTrace();
                throw new JvnException("(jvnLookupObject) Problem connecting to the coord server - assumed broken");
            }
        }
    }

    /**
     * Get latest object from Remote server using name
     *
     * @param jon : the JVN object name
     * @return the JVN object
     * @throws RemoteException, JvnException
     **/
    private JvnObject lookupObjectOnCoordServer(String jon) throws RemoteException, JvnException {
        JvnObject obj = remoteCoord.jvnLookupObject(jon, this);
        if (obj != null) {
            int id = obj.jvnGetObjectId();
            addJvnObjectToCache(id, obj);
        }
        return obj;

    }

    /**
     * Associate a symbolic name with a JVN object
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @throws JvnException, RemoteException
     **/
    private void registerObject(String jon, JvnObject jo)
            throws jvn.JvnException, RemoteException {
        // Associate object to name on coordinator server
        remoteCoord.jvnRegisterObject(jon, jo, this);
        // Get id on coord server and save it to map
        int id = jo.jvnGetObjectId();
        addJvnObjectToCache(id, jo);
    }

    /**
     * Get a Read lock on a JVN object
     *
     * @param joi : the JVN object identification
     * @return the current JVN object state
     * @throws JvnException
     **/
    public Serializable jvnLockRead(int joi)
            throws JvnException {
        try {
            /*
             * Lock read the object with the given identifier, and get current state
             */
            Serializable obj = remoteCoord.jvnLockRead(joi, this);
            // Make sure object is added to cache
//            addJvnObjectToCache(joi, (JvnObject) obj);
            return obj;
        } catch (RemoteException re) {
            System.out.println("(jvnLockRead) Problem connecting to the coord server, this could be due to overloaded queue or broken remote server - retrying again");
            try {
                Thread.sleep(100);
                Serializable obj = remoteCoord.jvnLockRead(joi, this);
                return obj;
            } catch (InterruptedException ie) {
                ie.printStackTrace();
                // Restore the interrupted status (otherwise this state is cleared)
                Thread.currentThread().interrupt();
                throw new JvnException("(jvnLockRead) InterruptedException (see stack trace)");
            } catch (RemoteException re2) {
                System.out.println("(jvnLockRead) Problem connecting to the coord server - assumed broken");
                re2.printStackTrace();
                throw new JvnException("(jvnLockRead) Problem connecting to the coord server - assumed broken");
            }
        }

    }

    /**
     * Get a Write lock on a JVN object
     *
     * @param joi : the JVN object identification
     * @return the current JVN object state
     * @throws JvnException
     **/
    public Serializable jvnLockWrite(int joi)
            throws JvnException {
        try {
            // Lock read the object with the given identifier
            Serializable obj = remoteCoord.jvnLockWrite(joi, this);
            // Make sure object is added to cache
//            addJvnObjectToCache(joi, (JvnObject) obj);
            return obj;
        } catch (RemoteException re) {
            System.out.println("(jvnLockWrite) Problem connecting to the coord server, this could be due to overloaded queue or broken remote server - retrying again");
            try {
                Thread.sleep(100);
                Serializable obj = remoteCoord.jvnLockWrite(joi, this);
                return obj;
            } catch (InterruptedException ie) {
                ie.printStackTrace();
                // Restore the interrupted status (otherwise this state is cleared)
                Thread.currentThread().interrupt();
                throw new JvnException("(jvnLockWrite) InterruptedException (see stack trace)");
            } catch (RemoteException re2) {
                re2.printStackTrace();
                throw new JvnException("(jvnLockWrite) Problem connecting to the coord server - assumed broken");
            }
        }
    }


    /**
     * Invalidate the Read lock of the JVN object identified by id
     * called by the JvnCoord
     *
     * @param joi : the JVN object id
     * @return void
     * @throws java.rmi.RemoteException,JvnException
     **/
    public void jvnInvalidateReader(int joi)
            throws java.rmi.RemoteException, jvn.JvnException {
        // objectOfId may not contain object if object cache has been cleared
        if (objectOfId.containsKey(joi)) {
            objectOfId.get(joi).jvnInvalidateReader();
        } else {
            throw new JvnNoObjCacheException("Cannot invalidate reader as object cache does not exist - this maybe due to it being removed during cache clearing");
        }
    }

    ;

    /**
     * Invalidate the Write lock of the JVN object identified by id
     *
     * @param joi : the JVN object id
     * @return the current JVN object state
     * @throws java.rmi.RemoteException,JvnException
     **/
    public Serializable jvnInvalidateWriter(int joi)
            throws java.rmi.RemoteException, jvn.JvnException {
        // objectOfId may not contain object if object cache has been cleared
        if (objectOfId.containsKey(joi)) {
            return objectOfId.get(joi).jvnInvalidateWriter();
        } else {
            throw new JvnNoObjCacheException("Cannot invalidate writer as object cache does not exist - this maybe due to it being removed during cache clearing");
        }
    }

    ;

    /**
     * Reduce the Write lock of the JVN object identified by id
     *
     * @param joi : the JVN object id
     * @return the current JVN object state
     * @throws java.rmi.RemoteException,JvnException
     **/
    public Serializable jvnInvalidateWriterForReader(int joi)
            throws java.rmi.RemoteException, jvn.JvnException {

        // objectOfId may not contain object if object cache has been cleared
        if (objectOfId.containsKey(joi)) {
            return objectOfId.get(joi).jvnInvalidateWriterForReader();
        } else {
            throw new JvnNoObjCacheException("Cannot invalidate writer for reader as object cache does not exist - this maybe due to it being removed during cache clearing");
        }
    }

    ;

    public boolean jvnObjectCached(int joi){
        return (objectOfId.containsKey(joi));
    }

    // If object not added to cache, add it
//    private void addJvnObjectToCache(int id, JvnObject jo) {
//        // Clear object cache before adding an object to the map if limit reached
//        if (!objectOfId.containsKey(id)) {
//            if ((objectOfId.size() + 1) > OBJECT_CACHE_LIMIT) {
//                System.out.println("clear object cache");
//
//                clearJvnObjectCache();
//            }
//            objectOfId.put(id, jo);
//            System.out.println("objet - with id " + id  + " added to cache");
//        }
//    }

    // If object not added to cache, add it
    public void addJvnObjectToCache(int id, JvnObject jo) {
        // Clear object cache before adding an object to the map if limit reached
        if (!objectOfId.containsKey(id)) {
            if ((objectOfId.size() + 1) > OBJECT_CACHE_LIMIT) {
                clearJvnObjectCache();
            }
            objectOfId.put(id, jo);
        }
    }

    /*
     * Clear all jvn object caches saved. This is to prevent excessive copies of jvn objects that may no longer be needed
     */
    private void clearJvnObjectCache() {
        ArrayList<Integer> toRemove = new ArrayList<>();
        // For multithread environment
        for (Integer jvnObjectId : objectOfId.keySet()) {
            // Synchronized to make sure object is not in the middle of writing or reading
            synchronized (objectOfId.get(jvnObjectId)) {
                if (objectOfId.get(jvnObjectId).isTransactionBound()) {
                    System.out.println("Object could not be removed from cache as read or write lock is present, object id = " + jvnObjectId);
                } else {
//                    objectOfId.remove(jvnObjectId);
                    toRemove.add(jvnObjectId);
                    System.out.println("Object removed from cache = " + jvnObjectId);
                }
            }
        }
        objectOfId.keySet().removeAll(toRemove);
//         For single thread environment
//        objectOfId.clear();
    }

}

 
