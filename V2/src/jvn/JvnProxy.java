package jvn;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/*
 * This class acts as the invocation handler and sits between the dynamic proxy and the object
 */
public class JvnProxy implements InvocationHandler {

    private JvnObject jvnObj;

    private JvnProxy(JvnObject obj){
        this.jvnObj = obj;
    }

    public static Object newInstance(Serializable obj, String objName) {
        Object proxyObj = null;

        JvnObject jo = null;
        try {
            JvnServerImpl js = JvnServerImpl.jvnGetServer();
            jo = js.jvnLookupObject(objName);
            if (jo == null) {
                jo = js.jvnCreateObject(obj);
                jo.jvnUnLock();
                js.jvnRegisterObject(objName, jo);
            }
        } catch (JvnException e) {
            System.out.println("Exception looking up/creating/registering jvn object");
            e.printStackTrace();
            return null;
        }

        try{
            // Pass Class loader, interfaces and Invocation Handler to create dynamic proxy
            proxyObj = java.lang.reflect.Proxy.newProxyInstance(
                    obj.getClass().getClassLoader(),
                    obj.getClass().getInterfaces(),
                    new JvnProxy(jo));
        } catch (Exception exc){
            System.out.println("Exception creating dynamic proxy");
            exc.printStackTrace();
            return null;
        }

        return proxyObj;
    }

    /*
     * This method performs the invocations for this class as part of the invocation handler interface
     */
    @Override
    public Object invoke(Object obj, Method method, Object[] args) throws Throwable {

        // Check if the method has a JvnMethodType annotation
        if(method.isAnnotationPresent(JvnMethodType.class)){
            if(method.getAnnotation(JvnMethodType.class).type() == JvnMethodType.MethodType.READ){
                jvnObj.jvnLockRead();
            } else if (method.getAnnotation(JvnMethodType.class).type() == JvnMethodType.MethodType.WRITE) {
                jvnObj.jvnLockWrite();
            } else{
                throw new Exception("Method uses correct annotation class to invoke the jvn dynamic proxy - but not a correct type (see JvnMethodType class for method types)");
            }
            Object result = method.invoke(jvnObj.jvnGetSharedObject(), args);
            jvnObj.jvnUnLock();
            return  result;

        } else{
            throw new Exception("Method is not annotated correctly to invoke the jvn dynamic proxy");
        }
    }
}
