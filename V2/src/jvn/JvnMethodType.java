package jvn;

import java.lang.annotation.*;

/*
 * Annotate methods with the Read or Write method types if methods need to be invoked by the jvn proxy
 * (JvnProxy.invoke())
 */
@Retention(RetentionPolicy.RUNTIME) // Annotation available at execution time
@Target(ElementType.METHOD) // Available for methods
public @interface JvnMethodType {
    enum MethodType {
        READ,
        WRITE
    }

    MethodType type();
}
