package irc;

import jvn.JvnMethodType;

/*
 * Interface for sentence - write/read methods are annotated with methods which can be invoked by the invocation handler
 */
public interface ISentence {

    @JvnMethodType(type = JvnMethodType.MethodType.WRITE)
    void write(String text);

    @JvnMethodType(type = JvnMethodType.MethodType.READ)
    String read();

}
