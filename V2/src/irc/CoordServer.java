package irc;

import jvn.JvnCoordImpl;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class CoordServer {
    public static void main(String argv[]){
        try {
            JvnCoordImpl coordinator = new JvnCoordImpl();
            Registry registry = LocateRegistry.createRegistry(2000);
            registry.bind("RemoteCoordinator", coordinator);
            System.out.println("Coordinator is ready");
        } catch (Exception e) {
            System.out.println("Coordinator failed to launch");
            e.printStackTrace();
        }
    }
}
