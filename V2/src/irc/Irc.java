/***
 * Irc class : simple implementation of a chat using JAVANAISE 
 * Contact: 
 *
 * Authors: 
 */

package irc;

import java.awt.*;
import java.awt.event.*;


import jvn.*;

import javax.swing.plaf.synth.SynthLookAndFeel;
import java.io.*;


public class Irc {
    public TextArea text;
    public TextField data;
    Frame frame;
    ISentence sentence;

    /**
     * main method
     * create a Sentence object for representing the Chat application
     **/
    public static void main(String argv[]) {
        try {
            ISentence sentence = (ISentence)JvnProxy.newInstance(new Sentence(), "IRC");
            new Irc(sentence);
        } catch (Exception e) {
            System.out.println("IRC problem : " + e);
            System.out.println("IRC problem : " + e.getStackTrace()[0].getLineNumber());
        }
    }

    /**
     * IRC Constructor
     *
     * @param sentence the Sentence representing the Chat
     **/
    public Irc(ISentence sentence) {
        this.sentence = sentence;
        frame = new Frame();
        frame.setLayout(new GridLayout(1, 1));
        text = new TextArea(10, 60);
        text.setEditable(false);
        text.setForeground(Color.red);
        frame.add(text);
        data = new TextField(40);
        frame.add(data);
        Button read_button = new Button("read");
        read_button.addActionListener(new readListener(this));
        frame.add(read_button);
        Button write_button = new Button("write");
        write_button.addActionListener(new writeListener(this));
        frame.add(write_button);
        frame.setSize(545, 201);
        text.setBackground(Color.black);
        frame.setVisible(true);
    }
}


/**
 * Internal class to manage user events (read) on the CHAT application
 **/
class readListener implements ActionListener {
    Irc irc;

    public readListener(Irc i) {
        irc = i;
    }

    /**
     * Management of user events
     **/
    public void actionPerformed(ActionEvent e) {
        String s = irc.sentence.read();
        irc.data.setText(s);
        irc.text.append(s + "\n");
    }
}

/**
 * Internal class to manage user events (write) on the CHAT application
 **/
class writeListener implements ActionListener {
    Irc irc;

    public writeListener(Irc i) {
        irc = i;
    }

    /**
     * Management of user events
     **/
    public void actionPerformed(ActionEvent e) {
        String s = irc.data.getText();
        irc.sentence.write(s);
    }
}



