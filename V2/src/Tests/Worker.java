package Tests;

import irc.ISentence;
import irc.Sentence;
import jvn.JvnProxy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Worker {

    public static String OBJECT_PREFIX = "OBJ_";

    public static void main(String argv[]) {
        if(argv.length != 3){
            System.err.println("Invalid number of arguments, should be 3 but is " + argv.length);
        }
        List<ISentence> sentenceList = new ArrayList<>();
        int id = Integer.parseInt(argv[0]);
        int NB_OBJECTS = Integer.parseInt(argv[1]);
        int NB_INSTRUCTIONS = Integer.parseInt(argv[2]);
        Random random = new Random();
        for(int i = 0; i < NB_OBJECTS; i++){
            try {
                ISentence sentence = (ISentence) JvnProxy.newInstance(new Sentence(), OBJECT_PREFIX + i);
                sentenceList.add(sentence);
            } catch (Exception e) {
                System.out.println("Burst test problem on sentence instanciation in worker " + id + " : " + e);
            }
        }

        System.out.println("Worker #" + id + " starting work...");

        for(int i = 0; i < NB_INSTRUCTIONS; i++){

            //System.out.println("Instruction number = " + i);

            int sentenceNb = random.nextInt(sentenceList.size());
            ISentence sentence = sentenceList.get(sentenceNb);

            if(random.nextBoolean()){
                System.out.println("Worker #" + id + " reading sentence #"+sentenceNb+"... on instruction #" + i);
                String s = sentence.read();
                System.out.println("Worker #" + id + " read  sentence #"+sentenceNb+"... on instruction #" + i);

            }else{
                System.out.println("Worker #" + id + " writing sentence #"+sentenceNb+"... on instruction #" + i);
                String s = "Worker " + id + " wrote here on instruction #" + i;
                sentence.write(s);
                System.out.println("Worker #" + id + " wrote on sentence #"+sentenceNb+"... on instruction #" + i);
            }
        }

        System.out.println("Worker #" + id + " finished its work...");

        System.exit(0);
    }
}
