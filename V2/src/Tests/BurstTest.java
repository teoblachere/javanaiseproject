package Tests;

import irc.Sentence;
import jvn.JvnProxy;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;

public class BurstTest {

    public static int NB_CLIENTS = 5;
    public static int NB_OBJECTS = 2;
    public static int NB_INSTRUCTIONS = 60;

    public static void main(String argv[]) {
        Instant start = Instant.now();
        System.out.println("Creating sentences...");
        for(int i = 0; i < NB_OBJECTS; i++){
            try {
                JvnProxy.newInstance(new Sentence(), "OBJ_" + i);
            } catch (Exception e) {
                System.out.println("Error on sentence creation");
            }
        }
        ArrayList<Process> processes = new ArrayList<>();
        for (int i = 0; i < NB_CLIENTS; i++) {
            System.out.println("Starting worker #" + i);
            ProcessBuilder pb = new ProcessBuilder("java", "-Dfile.encoding=UTF-8", "-classpath", "bin", "Tests.Worker",
                    String.valueOf(i), String.valueOf(NB_OBJECTS), String.valueOf(NB_INSTRUCTIONS));
            pb.inheritIO();
            try {
                Process process = pb.start();
                processes.add(process);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Waiting for processes to end...");
        for(Process p : processes){
            p.onExit().join();
        }
        System.out.println("Job finished");
        String timeElapsed = Duration.between(start, Instant.now()).toString();
        System.out.println(NB_CLIENTS + " clients did " + NB_INSTRUCTIONS + " operations on " + NB_OBJECTS + " shared objects in " + timeElapsed.substring(2));
        System.exit(0);
    }
}
